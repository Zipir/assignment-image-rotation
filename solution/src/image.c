#include "image.h"

#include <assert.h>
#include <stddef.h>
#include <stdlib.h>

// Выделить память под изображение определённого размера
struct image new_image(uint64_t width, uint64_t height)
{
    return (struct image) {
        .width = width,
        .height = height,
        .data = calloc(width * height, sizeof(struct pixel))
    };
}

// Очистить выделенную под изображение память
void delete_image(struct image *img)
{
    if (img)
    {
        img->width = 0;
        img->height = 0;

        free(img->data);
        img->data = NULL;
    }
}


// Получить индекс пикселя через координаты
uint64_t get_pixel_index(const struct image *img, uint64_t x, uint64_t y)
{
    assert(img && "Image may not be NULL!");
    assert(x < img->width && y < img->height && "Pixel coordinates out of bounds!");

    return x + y * img->width;
}
// Получить пиксель через координаты
struct pixel * get_pixel(struct image *img, uint64_t x, uint64_t y)
{
    assert(img && "Image may not be NULL!");
    assert(x < img->width && y < img->height && "Pixel coordinates out of bounds!");

    return &img->data[x + y * img->width];
}
