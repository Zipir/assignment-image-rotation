#include "rotate.h"

#include <assert.h>

// Получить повёрнутое на 90 градусов влево (против часовой стрелки) изображение
struct image get_rotated_90_deg_left( struct image img, struct image* source)
{



    //           5 2
    // 3 4 5  -> 4 1
    // 0 1 2     3 0 
    uint64_t i = 0;
    for (uint64_t x = source->width ; x > 0; --x)
    {
        for (uint64_t y = 0; y < source->height; ++y)
        {
            struct pixel* pxl = get_pixel(source, x-1, y);
            *pxl = img.data[i];
            i++;
        }
    }

    return *source;
}
