#include "bmp.h"
#include <assert.h>
#include <file.h>
#include <malloc.h>


#define BMP_SIGNATURE 0x4D42
#define BMP_RGB_BIT_COUNT 24
#define BMP_PLANES 1
#define BMP_DIB_HEADER_SIZE 40

#define BMP_RESERVED 0
#define BMP_COMPRESSION 0
#define XPELS_PER_METER 0
#define YPELS_PER_METER 0
#define CLR_USED 0
#define CLR_IMPORTANT 0

#pragma pack(push, 1)
// Заголовок BMP файла
struct bmp_header
{
    uint16_t bfType;          // BMP сигнатура ("BM" или же 0x4D42)
    uint32_t bfileSize;       // Размер файла в байтах
    uint32_t bfReserved;      // Не используется
    uint32_t bDataOffset;     // Начало данных (всегда 54 = 14 + 40)

    /* DIB Header */
    uint32_t biSize;          // Размер DIB заголовка (40)
    uint32_t biWidth;         // Ширина изображения в пикселях 
    uint32_t biHeight;        // Высота изображения в пикселях
    uint16_t biPlanes;        // 1
    uint16_t biBitCount;      // Число бит на пиксель (24 для RGB)
    uint32_t biCompression;   // Формат сжатия (0, т.е. отсутствует, для RGB)
    uint32_t biSizeImage;     // Размер сжатого изображения (может быть 0 у RGB)
    uint32_t biXPelsPerMeter; // Горизонтальное разрешение
    uint32_t biYPelsPerMeter; // Вертикальное разрешение
    uint32_t biClrUsed;       // Используемые цвета
    uint32_t biClrImportant;  // Важные цвета (0 - все цвета)
};
#pragma pack(pop)


// Сообщения об ошибках чтения
static const char* const read_status_messages[] = {
        [READ_OK]                = "Read OK\n",
        [READ_INVALID_SIGNATURE] = "Invalid signature\n",
        [READ_INVALID_BITS]      = "Invalid bits\n",
        [READ_INVALID_HEADER]    = "Invalid BMP header\n"
};


// Сообщения об ошибках записи
static const char* const write_status_messages[] = {
        [WRITE_OK]    = "Write OK\n",
        [WRITE_ERROR] = "Write Error!\n"
};

// Получить размер padding в зависимости от ширины изображения
uint8_t get_padding(uint32_t width)
{
    return (4 - width * sizeof(struct pixel) % 4) % 4;
}
// Проверка успешности чтения файла
struct image image_from_bmp(const char* file_name)
{
    FILE* input_file;
    int open_status = open_file(&input_file, file_name, "rb");
    if(open_status == 0)
    {
        fprintf(stderr, "Can't open file");
    }
    else
    {
        fprintf(stdout, "File successfuly opened");
    }

    struct image input_image = {0};
    enum read_status read_status = from_bmp(input_file, &input_image);
    print_read_status(read_status);
    if(close_file(input_file) == 1)
    {
        fprintf(stderr, "Can't close file");
    }
    else
    {
        fprintf(stdout, "File successfuly closed");
    }
    
    return input_image;
}


// Проверка успешности записи файла
void image_to_bmp(struct image* output_image, const char* file_name)
{
    FILE* output_file;
    int open_status = open_file(&output_file, file_name, "wb");
    if(open_status == 0)
    {
        fprintf(stderr, "Can't open file");
    }
    else
    {
        fprintf(stdout, "File successfuly opened");
    }
    enum write_status write_status = to_bmp(output_file, output_image);
    print_write_status(write_status);
    if(close_file(output_file) == 1)
    {
        fprintf(stderr, "Can't close file");
    }
    else
    {
        fprintf(stdout, "File successfuly closed");
    }
}

// Проверка валидности BMP заголовка
enum read_status check_header(const struct bmp_header *header)
{
    if (header->bfType != BMP_SIGNATURE)
    {
        return READ_INVALID_SIGNATURE;
    }

    if (header->bDataOffset != sizeof(struct bmp_header))
    {
        return READ_INVALID_HEADER;
    }

    if (header->biSize != BMP_DIB_HEADER_SIZE)
    {
        return READ_INVALID_HEADER;
    }

    if (header->biPlanes != BMP_PLANES)
    {
        return READ_INVALID_HEADER;
    }

    if (header->biBitCount != BMP_RGB_BIT_COUNT)
    {
        return READ_INVALID_BITS;
    }

    return READ_OK;
}


// Чтение изображения из открытого файла в формате bmp
enum read_status from_bmp(FILE *in, struct image *img)
{
    assert(img && "Image may not be NULL!");

    struct bmp_header header = {0};
    if (fread(&header, sizeof(struct bmp_header), 1, in) != 1)
    {
        return READ_INVALID_HEADER;
    }

    enum read_status header_status = check_header(&header);
    if (header_status != READ_OK)
    {
        return header_status;
    }

    *img = new_image(header.biWidth, header.biHeight);

    uint64_t i = 0;
    uint32_t padding = get_padding(header.biWidth);
    for(uint64_t y = 0; y < img->height; ++y)
    {
        for(uint64_t x = 0; x < img->width; ++x)
        {
            if (fread(img->data + i, sizeof(struct pixel), 1, in) != 1)
            {
                delete_image(img);
                return READ_INVALID_BITS;
            }
            ++i;
        }
        if (fseek(in, padding, SEEK_CUR) != 0){
            delete_image(img);
            return READ_INVALID_BITS;
        }
    }

    return READ_OK;
}

// Создаём заголовок из изображения
struct bmp_header create_bmp_header(const struct image *img)
{
    uint32_t width = img->width;
    uint32_t height = img->height;
    uint32_t bmp_header_size = sizeof(struct bmp_header);
    uint32_t image_size = (width * sizeof(struct pixel) + get_padding(width)) * height;
    return (struct bmp_header) {
        .bfType          = BMP_SIGNATURE,
        .bfileSize       = bmp_header_size + image_size,
        .bfReserved      = BMP_RESERVED,
        .bDataOffset     = bmp_header_size,
        .biSize          = BMP_DIB_HEADER_SIZE,
        .biWidth         = width,
        .biHeight        = height,
        .biPlanes        = BMP_PLANES,
        .biBitCount      = BMP_RGB_BIT_COUNT,
        .biCompression   = BMP_COMPRESSION,
        .biSizeImage     = image_size,
        .biXPelsPerMeter =  XPELS_PER_METER,
        .biYPelsPerMeter =  YPELS_PER_METER,
        .biClrUsed       = CLR_USED,
        .biClrImportant  = CLR_IMPORTANT
    };
}


// Запись изображения в открытый файл в формате bmp
enum write_status to_bmp(FILE *out, const struct image *img)
{
    struct bmp_header header = create_bmp_header(img);
    if(fwrite(&header, sizeof(struct bmp_header), 1, out) != 1)
    {
        return WRITE_ERROR;
    }

    uint8_t padding_byte = 0;
    uint32_t padding = get_padding(header.biWidth);
    uint64_t i = 0;
    for(uint64_t y = 0; y < img->height; ++y)
    {
        for(uint64_t x = 0; x < img->width; ++x)
        {
            if(fwrite(img->data + i, sizeof(struct pixel), 1, out) != 1)
            {
                return WRITE_ERROR;
            }
            ++i;
        }
        for (uint32_t j = 0; j < padding; ++j) 
        {
            if (fwrite(&padding_byte, sizeof(uint8_t), 1, out) != 1) 
            {
                return WRITE_ERROR;
            }
        }
    }
    return WRITE_OK;
}

// Вывести ошибку чтения
void print_read_status(enum read_status status)
{
    fprintf(stderr, "%s", read_status_messages[status]);
}

// Вывести ошибку записи
void print_write_status(enum write_status status)
{
    fprintf(stderr, "%s", write_status_messages[status]);
}
