#include "transform.h"

struct image image_transform(struct image input_image, struct image(transform)( struct image, struct image*)){

    if(input_image.width == 0 || input_image.height == 0 || !input_image.data){
        return new_image(0, 0);
    }

    // Повёрнутое изображение теперь размером не width x height, а height x width. Это не ошибка!
    struct image res = new_image(input_image.height, input_image.width);

struct image image_transformed = transform(input_image, &res);
if(image_transformed.width == 0 || image_transformed.height == 0 || !image_transformed.data){
return new_image(0, 0);
}

return image_transformed;
}
