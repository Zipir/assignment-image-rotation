#include "file.h"
#include <stdio.h>
#include <stdlib.h>

int open_file(FILE** file, const char* name, const char* mode) {
    *file = fopen(name, mode);
    if(!file)
    {
        return 0;
    }
    return 1;
}

int close_file(FILE* file) {
    return fclose(file);
}

