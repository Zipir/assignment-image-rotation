#include <stdio.h>

#include "bmp.h"
#include "image.h"
#include "rotate.h"
#include "transform.h"

int main(int argc, char **argv) {
    if (argc < 3) {
        fprintf(stderr, "Usage: ./image-transformer <source-image> <transformed-image>\n");
        return 1;
    }


    struct image source = image_from_bmp(argv[1]);

    if (!source.data) {
        delete_image(&source);
        fprintf(stderr, "Error with converting bmp to struct image\n");
        return 1;
    }

    struct image transformed = image_transform(source, get_rotated_90_deg_left);

    delete_image(&source);

    if (!transformed.data) {
        delete_image(&transformed);
        fprintf(stderr, "Transformation failed\n");
        return 1;
    }

    image_to_bmp(&transformed, argv[2]);
    delete_image(&transformed);

    return 0;
}
