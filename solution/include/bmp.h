#pragma once

#include <stdio.h>

#include "image.h"

struct image image_from_bmp(const char* file_name) ;

void image_to_bmp(struct image* output_image, const char* file_name);

/*  deserializer   */
// Ошибки чтения
enum read_status
{
    READ_OK = 0,
    READ_INVALID_SIGNATURE,
    READ_INVALID_BITS,
    READ_INVALID_HEADER
};

// Вывести ошибку чтения
void print_read_status(enum read_status status);

// Чтение изображения из открытого файла в формате bmp
enum read_status from_bmp(FILE *in, struct image *img);

/*  serializer   */

// Ошибки записи
enum write_status  
{
    WRITE_OK = 0,
    WRITE_ERROR
};



// Вывести ошибку записи
void print_write_status(enum write_status status);

// Запись изображения в открытый файл в формате bmp
enum write_status to_bmp(FILE *out, const struct image *img);
