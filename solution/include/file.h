
#ifndef ASSIGNMENT_IMAGE_ROTATION_FILE_H
#define ASSIGNMENT_IMAGE_ROTATION_FILE_H

#include <stdio.h>

int open_file(FILE** file, const char* name, const char* mode);

int close_file(FILE* file);

#endif //ASSIGNMENT_IMAGE_ROTATION_FILE_H
