#pragma once

#include "image.h"

// Получить повёрнутое на 90 градусов влево (против часовой стрелки) изображение
struct image get_rotated_90_deg_left( struct image img, struct image* source);
