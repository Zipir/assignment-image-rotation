#pragma once

#include "image.h"

struct image image_transform( struct image input, struct image(transform)( struct image, struct image*));
