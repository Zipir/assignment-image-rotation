#pragma once

#include <stdint.h>

// RGB цвет пикселя
#pragma pack(push, 1)
struct pixel { uint8_t b, g, r; };
#pragma pack(pop)

// Внутренняя репрезентация изображения
struct image 
{
    uint64_t width, height;
    struct pixel *data;
};

// Выделить память под изображение определённого размера
struct image new_image(uint64_t width, uint64_t height);
// Очистить выделенную под изображение память
void delete_image(struct image *img);

// Получить индекс пикселя через координаты
uint64_t get_pixel_index(const struct image *img, uint64_t x, uint64_t y);
// Получить пиксель через координаты
struct pixel * get_pixel(struct image *img, uint64_t x, uint64_t y);
